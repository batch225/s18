// Function Parameters and Arguments

// "name" is called a parameter that acts as a named variable that exists only inside of the function

// "Nehemiah" - argument - it is the information/data provided directly into the function

function printInput(name) {
	console.log(`My name is ${name}`);
};

printInput("Nehemiah");
printInput("Ron");

let sampleVariable = "Yui";

printInput(sampleVariable);

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log(`The remainder of ${num} divided by 8 is: ${remainder}`);
	let isDivisibleBy8 = remainder === 0;
	console.log(`Is ${num} divisible by 8?`);
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// Function as Arguments

function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed.");
};

function invokeFunction(argumentFunction){
	argumentFunction();
	console.log("This code comes from invokeFunction");
};

invokeFunction(argumentFunction);

function getAverage(num1, num2, num3) {

	let average = (num1 + num2 + num3) / 3;

	console.log(average);
	
}

function printWords(getAverage) {
	console.log("This is your Average");

	getAverage(10, 20, 30);

	console.log("Thank you!");
}

// Multiple Arguments - will correspond to the number of "Parameters" declared in a function in succeeding order.

function createFullName(firstName, middleName, lastName) {
	console.log(`${firstName} ${middleName} ${lastName}`);
}

createFullName("Juan", "Dela");
createFullName("Juan", "Dela", "Cruz");
createFullName("Juan", "Dela", "Cruz", "Hello");

let firstName = "John";
let middleName = "Dela";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

// Return Statements - allows us to output a value from a function to be passed to the line/block of code that invoked/ called function.

// The "return" statement also stops the execution of the function and any code after the return statement will not be executed.

function returnFullName(firstName, middleName, lastName) {

	return console.log(`${firstName} ${middleName} ${lastName}`);

	console.log("This message will not be printed");
}

returnFullName("Joe", "Jayson", "Helberg");

function returnAddress(city, country) {
	let fullAddress = `${city}, ${country}`;
	return console.log(fullAddress);
}

returnAddress("Cebu City", "Philippines");



 

