
	// 1.  Create a function which will be able to add two numbers.
	// 	-Numbers must be provided as arguments.
	// 	-Display the result of the addition in our console.
	// 	-function should only display result. It should not return anything

	function addition(num1, num2) {
		console.log(`Displayed sum of ${num1} and ${num2}`);
		console.log(num1 + num2);
	}


	// 	Create a function which will be able to subtract two numbers.
	// 	-Numbers must be provided as arguments.
	// 	-Display the result of subtraction in our console.
	// 	-function should only display result. It should not return anything.

	function subtraction(num1, num2) {
		console.log(`Displayed difference of ${num1} and ${num2}`);
		console.log(num1 - num2);
	}


	// 	-invoke and pass 2 arguments to the addition function
	// 	-invoke and pass 2 arguments to the subtraction function

	addition(5,15);
	subtraction(20,5);

	// 2.  Create a function which will be able to multiply two numbers.
	// 		-Numbers must be provided as arguments.
	// 		-Return the result of the multiplication.

	function multiplication(num1, num2) {
		console.log(`The product of ${num1} and ${num2}:`);
		return num1 * num2;
	}



	// 	Create a function which will be able to divide two numbers.
	// 		-Numbers must be provided as arguments.
	// 		-Return the result of the division.

	function division(num1, num2) {
		console.log(`The quotient of ${num1} and ${num2}:`);
		return num1 / num2;
	}


	//  Create a global variable called outside of the function called product.
	// 		-This product variable should be able to receive and store the result of multiplication function.
	// 	Create a global variable called outside of the function called quotient.
	// 		-This quotient variable should be able to receive and store the result of division function.
	// 	Log the value of product variable in the console.
	// 	Log the value of quotient variable in the console.

	let product = multiplication(50, 10);
	console.log(product);

	let quotient = division(50, 10);
	console.log(quotient);


	// 3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
	// 		-a number should be provided as an argument.
	// 		-look up the formula for calculating the area of a circle with a provided/given radius.
	// 		-look up the use of the exponent operator.
	// 		-you can save the value of the calculation in a variable.
	// 		-return the result of the area calculation.

	function areaCalc(radius) {
		console.log(`The result of getting the area of a circle with ${radius} radius:`);
		return Math.PI * Math.pow(radius, 2);
	}

	// 	Create a global variable called outside of the function called circleArea.
	// 		-This variable should be able to receive and store the result of the circle area calculation.
	// Log the value of the circleArea variable in the console.

	let circleArea = areaCalc(15).toFixed(2);
	console.log(circleArea);

	// 4. 	Create a function which will be able to get total average of four numbers.
	// 		-4 numbers should be provided as an argument.
	// 		-look up the formula for calculating the average of numbers.
	// 		-you can save the value of the calculation in a variable.
	// 		-return the result of the average calculation.

	function totalAverage(num1, num2, num3, num4) {
		console.log(`The average of ${num1}, ${num2}, ${num3} and ${num4}:`);
		return (num1 + num2 + num3 + num4) / 4;
	}

	//     Create a global variable called outside of the function called averageVar.
	// 		-This variable should be able to receive and store the result of the average calculation
	// 		-Log the value of the averageVar variable in the console.

	let averageVar = totalAverage(20,40,60,80);
	console.log(averageVar);

	// 5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
	// 		-this function should take 2 numbers as an argument, your score and the total score.
	// 		-First, get the percentage of your score against the total. You can look up the formula to get percentage.
	// 		-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
	// 		-return the value of the variable isPassed.
	// 		-This function should return a boolean.

	function passingPercentage(score, totalScore) {
		console.log(`Is ${score}/${totalScore} a passing score?`);
		percentage = (score / totalScore) * 100;
		isPassed = percentage >= 75;
		return isPassed
	}

	// 	Create a global variable called outside of the function called isPassingScore.
	// 		-This variable should be able to receive and store the boolean result of the checker function.
	// 		-Log the value of the isPassingScore variable in the console.

	let isPassingScore = passingPercentage(38, 50);
	console.log(isPassingScore);


